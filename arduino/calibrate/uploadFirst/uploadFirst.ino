// Original Source: https://lygte-info.dk/project/PrecisionTime%20UK.html
// Permalink: http://web.archive.org/web/20200301143714/https://lygte-info.dk/project/PrecisionTime%20UK.html 
#include <PrecisionTime.h>

PrecisionTime precisionTime(PRECISIONTIME_EEPROM_ADDRESS);

PRECISION_TIMER_INTR(precisionTime)

void setup() {
  precisionTime.begin();
  Serial.begin(9600);
  Serial.println("Test precision time");
}

void loop() {
  precisionTime.update();

  if (Serial.available() > 0) {
    switch (Serial.read()) {
      case '#': precisionTime.startCalibration(); break;
    }
  }
}
