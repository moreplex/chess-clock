package dk.hkj.main;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.fazecast.jSerialComm.SerialPort;

public class Main {
	static Main main = null;
	public JFrame frame;
	private SerialInterface serialInterface;
	private List<Port> portList;
	private JComboBox<Port> portComboBox;
	private JComboBox<Integer> baudrateComboBox;
	private JTextField textField;
	private JTextArea logTextArea;
	private JTextField logFileTextField;
	private JLabel calStatusLabel;
	private JButton startButton;
	private JButton calButton;
	private JButton leaveButton;
	private StringBuilder logStringBuilder = new StringBuilder();
	private Port port = null;
	private String initString = "#";
	private long timeStartPC;
	private long timeStartArduino;
	private long runTimePC;
	private long runTimeArduino;
	private long deltaTime;
	private boolean done = true;
	private long calibrationFactor = 0;
	private long currentCalibration = 0;
	private long deltaDelta = 0;
	private List<Long> lastFactors = new ArrayList<Long>();
	private boolean nonCalibrationMode = false;
	private Writer logFile = null;
	private SimpleDateFormat clockFormat = new SimpleDateFormat("HH:mm:ss");
	private NumberFormat nflog;
	private String deli = "";

	class Port {
		String name;
		SerialPort port;

		Port(String name, SerialPort port) {
			this.name = name;
			this.port = port;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	private Main() {
		serialInterface = new SerialInterface();

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	// Preselect a port with a likely Adrduino name
	private boolean selectPort(String name) {
		for (Port p : portList) {
			if (p.name.contains(name)) {
				portComboBox.setSelectedItem(p);
				return true;
			}
		}
		return false;
	}

	private void createAndShowGUI() {
		// Create and set up the window.
		frame = new JFrame("Arduino PrecisionTime  calibration");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
			}

			@Override
			public void windowClosing(WindowEvent arg0) {
				if (!done) { // Still in calibration mode, get out!
					serialInterface.writeln("q");
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
					}
				}
				done = true;
				serialInterface.close();
			}
		});
		Dimension d = new Dimension(800, 400);
		frame.setMinimumSize(d);
		frame.setMaximumSize(d);
		frame.setPreferredSize(d);

		JPanel panel = new JPanel();
		BoxLayout bl = new BoxLayout(panel, BoxLayout.PAGE_AXIS);
		frame.add(panel);
		panel.setLayout(bl);

		portList = new ArrayList<Port>();
		for (SerialPort sp : serialInterface.getCommPorts()) {
			portList.add(new Port(sp.getDescriptivePortName(), sp));
		}
		portList.sort(new Comparator<Port>() {

			@Override
			public int compare(Port o1, Port o2) {
				return o1.port.getSystemPortName().compareTo(o2.port.getSystemPortName());
			}
		});

		JPanel comPortPanel = new JPanel();
		panel.add(comPortPanel);

		comPortPanel.add(new JLabel("Select com port for Arduino: "));
		portComboBox = new JComboBox<Port>(portList.toArray(new Port[0]));
		comPortPanel.add(portComboBox);
		if (!selectPort("Leonardo")) {
			selectPort("CH340");
		}

		baudrateComboBox = new JComboBox<Integer>(new Integer[] { 4800, 9600, 19200, 38400, 76800, 115200 });
		baudrateComboBox.setSelectedIndex(1);
		comPortPanel.add(baudrateComboBox);

		JPanel calStartPanel = new JPanel();
		panel.add(calStartPanel);

		calStartPanel.add(new JLabel("Calibration init string:"));

		textField = new JTextField(5);
		textField.setText(initString);
		calStartPanel.add(textField);

		calStartPanel.add(new JLabel("Log file:"));
		logFileTextField = new JTextField(20);
		calStartPanel.add(logFileTextField);

		startButton = new JButton("Start");

		startButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Runnable run = new Runnable() {
					@Override
					public void run() {
						startCalibration();
					}
				};
				logStringBuilder.setLength(0);
				port = (Port) portComboBox.getSelectedItem();
				initString = textField.getText().trim();
				done = false;
				Thread t = new Thread(run);
				t.setDaemon(true);
				t.start();
				t.setPriority(Thread.MAX_PRIORITY);
				startButton.setEnabled(false);
			}
		});
		calStartPanel.add(startButton);

		JPanel calStatusPanel = new JPanel();
		panel.add(calStatusPanel);

		calStatusLabel = new JLabel();
		calStatusPanel.add(calStatusLabel);

		JPanel calPanel = new JPanel();
		panel.add(calPanel);
		calButton = new JButton("Calibrate");
		calButton.setEnabled(false);
		calButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				done = true;
			}
		});
		calPanel.add(calButton);

		leaveButton = new JButton("Leave calibration");
		calPanel.add(leaveButton);
		leaveButton.setEnabled(false);
		leaveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				calibrationFactor = 0;
				done = true;
			}
		});

		JPanel logPanel = new JPanel();
		panel.add(logPanel);

		logTextArea = new JTextArea(10, 60);
		JScrollPane scroll = new JScrollPane(logTextArea);
		logPanel.add(scroll);

		// Display the window.
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		javax.swing.Timer timer = new javax.swing.Timer(300, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateUI();
			}

		});
		timer.setInitialDelay(1000);
		timer.start();

	}

	void updateUI() {
		startButton.setEnabled(done);
		leaveButton.setEnabled(!done);
		if (done) {
			calButton.setEnabled(false);
		}
	}

	void updateLog() {
		logTextArea.setText(logStringBuilder.toString());
	}

	synchronized void log(String msg) {
		logStringBuilder.append(msg + "\n");

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				updateLog();
			}
		});

	}

	boolean waitFor(int timeout, String msg) {
		String str = serialInterface.readln(300); // Flush serial port
		while (timeout > 0) {
			if (msg != null && str.equals(msg)) {
				return true;
			}
			if (str.length() > 0) {
				log("Rx: " + str);
			}
			str = serialInterface.readln(300);
			timeout -= 300;
		}
		return false;

	}

	long readCalibration() {
		try {
			nonCalibrationMode = false;
			String str = serialInterface.writeRead(2500, "f");
			if (str.length() == 0) {
				nonCalibrationMode = true;
				return 0;
			}
			if (str.charAt(0) == 'X') {
				nonCalibrationMode = true;
				str = str.substring(1);
			}
			return Long.parseLong(str);
		} catch (Exception e) {
			return 0;
		}
	}

	void startCalibration() {
		try {
			serialInterface.connect(port.port);
			serialInterface.setBaudRate((Integer) baudrateComboBox.getSelectedItem());
			log("Start calibration using port: " + port.name + " with " + (Integer) baudrateComboBox.getSelectedItem()
					+ " baud");
			try {
				Thread.sleep(2000);
			} catch (Exception e) {
			}
			if (!serialInterface.online()) {
				log("Failed to open port");
				return;
			}
			waitFor(200, null);
			log("Entering calibration mode with \"" + initString + "\"");
			serialInterface.writeln(initString);
			if (!waitFor(2000, "StartCalibration")) {
				// Lets try once more
				serialInterface.writeln(initString);
				if (!waitFor(2000, "StartCalibration")) {
					log("Missing response from Arduino");
					return;
				}
			}

			serialInterface.setBaudRate(76800);
			log("Switching to high speed");
			if (!waitFor(500, "StartCalibration fast")) {
				log("Missing response from Arduino at high speed");
				return;
			}
			log("In high speed mode");
			currentCalibration = readCalibration();
			log("Current calibration factor: " + (currentCalibration / 100.0) + " sec/day"
					+ (nonCalibrationMode ? ", but not used for this test" : ""));
			log("Starting calibration");
			serialInterface.writeln("!");
			timeStartPC = 0;
			timeStartArduino = 0;

			String logFileName = logFileTextField.getText();
			if (logFileName != null && logFileName.trim().length() > 0) {
				nflog = (DecimalFormat) NumberFormat.getNumberInstance();
				deli = (new DecimalFormatSymbols()).getDecimalSeparator() == ',' ? ";" : ",";
				nflog.setGroupingUsed(false);
				nflog.setMaximumFractionDigits(3);
				nflog.setMinimumFractionDigits(3);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
				String filename = logFileName + "-" + sdf.format(new Date()) + ".csv";
				try {
					logFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename)));
					logFile.write("clock" + deli + "hours" + deli + "PCtime" + deli + "ArduinoTime" + deli + "delta"
							+ deli + "day" + deli + "dayDelta\r\n");
				} catch (IOException e) {
					logFile = null;
				}
			}
			while (!done) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (!done) {
					timeCalibration(false);
				}
			}

			try {
				if (logFile != null) {
					logFile.close();
					logFile = null;
				}
			} catch (IOException e) {
			}

			long c = currentCalibration + calibrationFactor;
			if (Math.abs(c) > 60 * 60 * 1000) { // Calibration factor is ridiculous
				log("*** ERROR *** Calibration factor is to large: " + (c / 100.0) + " limit is 1 hour/day");
				return;
			}
			serialInterface.writeln("c" + c);
			long cc = readCalibration();
			log("New calibration factor: " + (cc / 100.0) + " sec/day");
			if (cc != c) {
				log("*** ERROR *** failed to program calibration factor " + (c / 100.0));
			}
			serialInterface.writeln("q");
			log("Switching to low speed and leaving calibration");
		} finally {
			serialInterface.setBaudRate((Integer) baudrateComboBox.getSelectedItem());
			serialInterface.close();
			done = true;
		}
	}

	private void timeCalibration(boolean cal) {

		runTimeArduino = Long.parseLong(serialInterface.writeRead(1500, "?")) * 1000 - timeStartArduino;
		runTimePC = System.currentTimeMillis() - timeStartPC;
		deltaTime = runTimeArduino - runTimePC;
		if (timeStartPC == 0) {
			timeStartPC = System.currentTimeMillis();
			timeStartArduino = runTimeArduino;
			return;
		}

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				updateTime();
			}
		});

	}

	void updateTime() {
		if (done)
			return;
		String s = "Calibration has been running for ";
		if (runTimePC < 600000L) {
			s += runTimePC / 1000 + " seconds";
		} else if (runTimePC < 36000000L) {
			s += runTimePC / 60000L + " minutes";
		} else {
			s += runTimePC / 3600000L + " hours";
		}

		s += "  ---  ";

		long day = deltaTime * 24L * 60L * 60L * 1000L / runTimePC;
		lastFactors.add(day);
		if (lastFactors.size() > 20) {
			lastFactors.remove(0);
		}
		long minL = Long.MAX_VALUE;
		long maxL = Long.MIN_VALUE;
		for (Long l : lastFactors) {
			if (l > maxL)
				maxL = l;
			if (l < minL)
				minL = l;
		}
		deltaDelta = (maxL - minL) / 2;
		if (Math.abs(deltaTime) < 5 || runTimePC < 30000L) {
			s += "Time difference is too small for any estimations (" + deltaTime + "ms)";
		} else {
			s += "Arduino is " + Math.abs(deltaTime) + "ms " + (deltaTime > 0 ? "ahead" : "behind");
			if (Math.abs(day) < 2000) {
				s += ", this is " + day + " (+/-" + deltaDelta + ") milliseconds in a day";
			} else {
				s += ", this is " + (day / 100) / 10.0 + " (+/-" + deltaDelta + "ms) seconds in a day";
			}
		}

		calStatusLabel.setText(s);
		if (Math.abs(deltaTime) > 10 && deltaDelta < 3000 && runTimePC > 30000L && leaveButton.isEnabled()) {
			calButton.setEnabled(!nonCalibrationMode);
			calibrationFactor = (int) (day / 10);
		}

		if (logFile != null) {
			try {
				logFile.write(clockFormat.format(new Date()) + deli + nflog.format(runTimePC / (1000.0 * 60 * 60))
						+ deli + runTimePC + deli + runTimeArduino + deli + deltaTime + deli
						+ (deltaTime * 24L * 60L * 60L * 1000L / runTimePC) + deli + deltaDelta + "\r\n");
				logFile.flush();
			} catch (IOException e) {
			}

		}
	}

	public static void main(String[] args) {
		System.out.println("Starting");
		main = new Main();
	}

}
