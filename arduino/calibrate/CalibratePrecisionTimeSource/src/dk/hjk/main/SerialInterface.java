package dk.hkj.main;

import java.util.Arrays;

// http://fazecast.github.io/jSerialComm/javadoc/com/fazecast/jSerialComm/package-summary.html
import com.fazecast.jSerialComm.SerialPort;

public class SerialInterface {

	private SerialPort serialPort = null;

	public SerialInterface() {

	}

	public synchronized SerialPort[] getCommPorts() {
		return SerialPort.getCommPorts();
	}

	public synchronized boolean connect(String port) {
		close();
		try {
			serialPort = SerialPort.getCommPort(port);
			return serialPort.openPort();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;

	}

	public synchronized void setBaudRate(int baud) {
		serialPort.setBaudRate(baud);
	}

	public synchronized boolean connect(SerialPort port) {
		close();
		try {
			serialPort = port;
			return serialPort.openPort();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;

	}

	public synchronized boolean online() {
		return serialPort != null && serialPort.isOpen();
	}

	public synchronized void close() {
		if (serialPort != null) {
			serialPort.closePort();
			serialPort = null;
		}
	}

	public synchronized boolean writeln(String value) {
		byte[] buffer = (value + "\n").getBytes();
		return serialPort.writeBytes(buffer, buffer.length) > 0;
	}

	private void sleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
		}
	}

	private synchronized int readByte() {
		byte[] buffer = new byte[1];
		if (serialPort.readBytes(buffer, 1) < 1)
			return -1;
		return buffer[0];
	}

	public synchronized String readln(int timeout) {
		byte[] buffer;
		buffer = new byte[1000];
		int c;
		int n = 0;
		int tt = 0;
		boolean done = false;
		do {
			c = readByte();
			if (c == '\n') {
				done = true;
			}
			if (c >= ' ') {
				buffer[n++] = (byte) c;
				tt = 0;
			} else {
				sleep(1);
				tt++;
				if (tt > timeout)
					done = true;
			}

		} while (n < buffer.length && !done);
		String s = new String(Arrays.copyOf(buffer, n));
		return s;
	}

	public synchronized boolean dataReady() {
		if (!online())
			return false;
		return serialPort.bytesAvailable() > 0;
	}

	public synchronized String writeRead(int timeout, String cmd) {
		writeln(cmd);
		return readln(timeout);
	}

}
