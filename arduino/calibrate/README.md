# Calibration of the onboard Arduino clock

This programme compares the onboard Arduino clock to the PC clock it is connected to. Ideally this should be run for longer than 1 h. This means that it can only be accurate as the computer clock. 

- [x] Calibrated > 1h

Here's the original link: https://lygte-info.dk/project/PrecisionTime%20UK.html 