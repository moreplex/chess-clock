#include <Countimer.h>
#include <Wire.h>
#include <hd44780.h>
#include <hd44780ioclass/hd44780_I2Cexp.h>

hd44780_I2Cexp lcd; // declare lcd obj

// rocker switch
short int switchPin = 0; // determine rocker switch state
unsigned short int initSwitch = digitalRead(switchPin);

// LCD geometry
const short int LCD_COLS = 16;
const short int LCD_ROWS = 2;

// Flag symbol on LCD
byte flag[8] = {
  0b11111,
  0b11111,
  0b11111,
  0b11111,
  0b10000,
  0b10000,
  0b10000,
  0b10000
};

// Timers 1 and 2, left and right
Countimer timer1;
Countimer timer2;

void setup() {
  Serial.begin(9600);
  lcd.begin(LCD_COLS, LCD_ROWS);

  pinMode(switchPin, INPUT);

  lcd.createChar(0, flag);

    // Set up count down timer with 1min and call method onComplete1() or onComplete2() when timer is complete.
    // 00h:05m:00s
  timer1.setCounter(0, 1, 0, timer1.COUNT_DOWN, onComplete1);
  timer2.setCounter(0, 1, 0, timer2.COUNT_DOWN, onComplete2);

    // Print current time every 1s on serial port by calling method refreshClock().
  //timer1.setInterval(refreshClock, 1000);
  //timer2.setInterval(refreshClock, 1000);

  while (digitalRead(switchPin) == initSwitch)
  {
    Serial.println("Ready");
    lcd.setCursor(0,0);
    lcd.print("Ready");
  }

  lcd.clear();

  // Print current time every 1s on serial port by calling method refreshClock().
  timer1.setInterval(refreshClock1, 500);
  timer2.setInterval(refreshClock2, 500);
}

void refreshClock1() {
  Serial.println(timer1.getCurrentTime()); // print current time
  lcd.setCursor(0,1);
  lcd.print(timer1.getCurrentTime());
}

void refreshClock2() {
  Serial.println(timer2.getCurrentTime()); // print current time
  lcd.setCursor(8,0);
  lcd.print(timer2.getCurrentTime());
}

void onComplete1() {
  //Serial.println("Complete1!!!");
  lcd.setCursor(0,1);
  lcd.print("00:00:00");
  lcd.setCursor(0,0);
  lcd.write((uint8_t)0); // print flag
}

void onComplete2(){
  //Serial.println("Complete1!!!");
  lcd.setCursor(8,0);
  lcd.print("00:00:00");
  lcd.setCursor(14,1);
  lcd.write((uint8_t)0); // print flag
}

void loop() {
  // Start timer
  timer1.run();
  timer2.run();

  if (!timer1.isCounterCompleted() && !timer2.isCounterCompleted())
  {
    if (digitalRead(switchPin) == initSwitch) 
    {
      timer2.pause();
      timer1.start();
    }
  
    else 
    {
      timer2.start();
      timer1.pause();
    }
  }

  else
  {
    timer1.pause();
    timer2.pause();
  }

    // Now timer is running and listening for actions.
    // If you want to start the timer, you have to call start() method.
    //if(!timer.isCounterCompleted()) 
    //{
    //  timer.start();
    //}
}
