const short int switchPin = 0;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(switchPin, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:

  if (digitalRead(switchPin) == HIGH) 
  {
    Serial.println("open");
  }

  else
  {
    Serial.println("closed");
  }
  delay(100);
}
