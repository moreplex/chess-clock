#include <ezButton.h> // button debounce

unsigned short int buttonPin = 1;
unsigned int pressNo = 0;

ezButton button(1);
//Switch pushButton = Switch(buttonPin);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  button.setDebounceTime(50);
}

void loop() {
  button.loop();
  // put your main code here, to run repeatedly:
  if (button.isPressed())
  {
    pressNo++;
  }

  Serial.println(pressNo);
}
