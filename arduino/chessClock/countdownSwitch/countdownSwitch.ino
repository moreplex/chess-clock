#include <Wire.h>
#include <hd44780.h>
#include <hd44780ioclass/hd44780_I2Cexp.h>

hd44780_I2Cexp lcd; // declare lcd obj

// rocker switch
short int switchPin = 0; // determine rocker switch state
unsigned short int initSwitch = digitalRead(switchPin);

// LCD geometry
const short int LCD_COLS = 16;
const short int LCD_ROWS = 2;

// Time keeping
unsigned int wTime = 6000; // white init time
long unsigned int check = millis();

void setup() {
  // put your setup code here, to run once:
  int status;
  
  Serial.begin(9600);
  pinMode(switchPin, INPUT);

  status = lcd.begin(LCD_COLS, LCD_ROWS);
  if (status) 
  {
    hd44780::fatalError(status);
  }

}

void loop() {
  // put your main code here, to run repeatedly:
  static unsigned long lastsecs = -1;
  unsigned long secs = 0;
  int status;

  if (digitalRead(switchPin) == initSwitch) 
  {
    Serial.println("open ");
    Serial.print(initSwitch);
    lcd.setCursor(0,1);
    
    if (secs == 0)
    {
      PrintUpTime(lcd, wTime);
    }

    else 
    {
      PrintUpTime(lcd, secs);
    }
  }

  else
  {
    Serial.println("closed ");
    Serial.print(initSwitch);
    secs = wTime - (millis() / 1000) - check;
    lcd.setCursor(8,0);
    PrintUpTime(lcd, secs);
  }
  
}

void PrintUpTime(Print &outdev, unsigned long secs)
{
unsigned int hr, mins, sec;

  // convert total seconds to hours, mins, seconds
  mins =  secs / 60;  // how many total minutes
  hr = mins / 60;   // how many total hours
  mins = mins % 60; // how many minutes within the hour
  sec = secs % 60;  // how many seconds within the minute
    
  // print uptime in HH:MM:SS format

  if(hr > 99)
    hr %= 100; // limit hr to 0-99

  // Print class does not support fixed width formatting
  // so insert a zero if number smaller than 10
    
  if(hr < 10)
    outdev.write('0');
  outdev.print((int)hr);
  outdev.write(':');
  if(mins < 10)
    outdev.write('0');
  outdev.print((int)mins);
  outdev.write(':');
  if(sec < 10)
    outdev.write('0');
  outdev.print((int)sec);
}
