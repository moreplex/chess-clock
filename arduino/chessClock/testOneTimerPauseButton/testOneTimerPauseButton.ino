#include <Countimer.h>
#include <Wire.h>
#include <hd44780.h>
#include <hd44780ioclass/hd44780_I2Cexp.h>
#include <ezButton.h> // button debounce

hd44780_I2Cexp lcd; // declare lcd obj

// rocker switch
short int switchPin = 0; // determine rocker switch state
unsigned short int initSwitch = digitalRead(switchPin);

// Toggle button
unsigned short int buttonPin = 1;
unsigned int pressNo = 0; // no. of times button pressed
ezButton button(1);

// LCD geometry
const short int LCD_COLS = 16;
const short int LCD_ROWS = 2;

// Flag symbol on LCD
byte flag[8] = {
  0b11111,
  0b11111,
  0b11111,
  0b11111,
  0b10000,
  0b10000,
  0b10000,
  0b10000
};

// Pause symbol on LCD
byte pauseSymb1[8] = {
  0b11011,
  0b11011,
  0b11011,
  0b11011,
  0b11011,
  0b11011,
  0b11011,
  0b00000
};

byte pauseSymb2[8] = {
  0b00000,
  0b11011,
  0b11011,
  0b11011,
  0b11011,
  0b11011,
  0b11011,
  0b11011
};

// Timers 1 and 2, left and right
Countimer timer1;

void setup() {
  Serial.begin(9600);
  lcd.begin(LCD_COLS, LCD_ROWS);

  pinMode(switchPin, INPUT);
  button.setDebounceTime(50);

  lcd.createChar(0, flag);
  lcd.createChar(1, pauseSymb1);

    // Set up count down timer with 1min and call method onComplete1() or onComplete2() when timer is complete.
    // 00h:05m:00s
  timer1.setCounter(0, 1, 0, timer1.COUNT_DOWN, onComplete1);

    // Print current time every 1s on serial port by calling method refreshClock().
  //timer1.setInterval(refreshClock, 1000);
  //timer2.setInterval(refreshClock, 1000);

  while (digitalRead(switchPin) == initSwitch)
  {
    Serial.println("Ready");
    lcd.setCursor(0,0);
    lcd.print("Ready");
    // print time1
    lcd.setCursor(0,1);
    lcd.print(timer1.getCurrentTime());
  }

  lcd.setCursor(0,0);
  lcd.print("     ");

  // Print current time every 1s on serial port by calling method refreshClock().
  timer1.setInterval(refreshClock1, 500);
}

void refreshClock1() {
  Serial.println(timer1.getCurrentTime()); // print current time
  lcd.setCursor(0,1);
  lcd.print(timer1.getCurrentTime());
}

void onComplete1() {
  //Serial.println("Complete1!!!");
  lcd.setCursor(0,1);
  lcd.print("00:00:00");
  lcd.setCursor(0,0);
  lcd.write((uint8_t)0); // print flag
}

void loop() {
  button.loop();
  // Start timer
  timer1.run();

  if (button.isPressed()) // while pause button is pressed
  {
    pressNo++;
  }
  
  Serial.println(pressNo);

  if (!timer1.isCounterCompleted())
  {
    if (pressNo % 2 == 1)
    {
      timer1.pause();
      Serial.println("paused");
      Serial.println(pressNo);
  
      lcd.setCursor(3,0);
      lcd.write((uint8_t)1); // write pause symbol1
  
      while (button.isPressed()) // while pause button is pressed
      {
        if (button.isPressed())
        {
          break;
        }
      }
    }

    else if (pressNo % 2 == 0)
    {
      lcd.setCursor(3,0);
      lcd.write(" "); // overwrite pause symbol
      if (initSwitch == 1)
      {
        if (digitalRead(switchPin) == initSwitch) 
        {
          timer1.pause();
        }
        
        else
        {
          timer1.start();
        }
      }
  
      else 
      {
        if (digitalRead(switchPin) == initSwitch) 
        {
          timer1.start();
        }
      
        else
        {
          timer1.pause();
        }
      }
    }
  }

  else
  { 
    timer1.pause();
  }
}
