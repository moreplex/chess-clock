#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2); // LCD address and 16x2 line display

  // 60000000 ms = 1000 min, wStart is whiteTime, bStart is blackTime
double wStart = 60000000; 

unsigned int wHrs = 0;
unsigned int wMins = 0;
unsigned int wSec = 0;
unsigned int wMil = 0;
double wCheck = millis();
double wElapsed = 0;
double wLeft = 0;

void setup() {
  lcd.init();
  lcd.clear();
  lcd.backlight(); // backlight on
}
  
void loop() {
    // print message
  lcd.setCursor(0,0);

    // calculate time w in millis
  wElapsed = millis() - wCheck;  
  wLeft = wStart - wElapsed;

    // 3600000 ms = 1 h
  wHrs = floor(wLeft / 3600000);
    // 60000 ms = 1 min
  wMins = floor(wLeft / 60000) - wHrs * 60; 
    //1000 ms = 1 s, and minus no. of seconds in wMins
  wSec = floor(wLeft / 1000) - wHrs * 3600 - wMins * 60; 
  wMil = wLeft - (wHrs * 3600000 + wMins*60000 + wSec*1000);
  
  if (wHrs < 10) {
    lcd.print("0"); // extra 0
  }
  lcd.print(wHrs); // no. of hrs
  lcd.print(":");
  if (wMins < 10) {
    lcd.print("0"); // extra 0
  }
  lcd.print(wMins); // no. of mins
  lcd.print(":");
  if (wSec < 10) {
    lcd.print("0"); // extra 0
  }
  lcd.print(wSec); // no. of seconds
  lcd.print(":");
  lcd.print(wMil); // no. of ms
  lcd.print(" ");

/*
  lcd.setCursor(0,1);
  lcd.print(wSec);
  lcd.print(" s");
*/
    // Not using player 2's time
  //lcd.setCursor(8,1);
  //lcd.print("00:05:00");
}
