#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2); // LCD address and 16x2 line display

void setup() {
  lcd.init();
  lcd.clear();
  lcd.backlight(); // backlight on
}
  
void loop() {
    // print message
  lcd.setCursor(0,0); //top left corner
  lcd.print("00:05:00");

  lcd.setCursor(8,1); //middle bottom line
  lcd.print("00:05:00");
}
