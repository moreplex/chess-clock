# Arduino Chess Clock Project

This is my GitLab project for an open-source accessibility friendly LCD chess clock. Here contains all my files, including Arduino code and 3D modelling files. Burnside High School users can open this link [here](https://docs.google.com/document/d/1zjAgO8MkTyFCxk8z0zq3mIT5XfA6HmcmHK-t5txtpM0/edit?usp=sharing) to view my Google document. This project aims to add Braille and audio functionality on the buttons, along with contrasting colours. 
